from datetime import datetime
import MySQLdb
import time, threading


class Config(threading.Thread):

    states = { 'control': 'manual', 'direction': 'stopped', 'delay': 0.05, 'time_in_state': 0 }

    db = MySQLdb.connect(host="localhost", user="root", passwd="1", db="pibot", )
    db.autocommit(True)

    stdb = db.cursor()  # Standard commands
    ddb  = db.cursor(MySQLdb.cursors.DictCursor) # To retrieve a dictionary (states)
    rddb = db.cursor(MySQLdb.cursors.DictCursor) # To retrieve a dictionary (requests)

    def state(self):
      return self.states

    def __init__(self):
        threading.Thread.__init__(self)
        self.running = True

    def prepare_database(self):
        # Clear the table of any unspent messages from a previous run
        self.stdb.execute("DELETE FROM requests")

    def __exit__(self):
        self.stdb.close()
        False

    def insert_request(self, part, vector, value):
        self.stdb.execute("""
           INSERT INTO requests (component, vector, value) VALUES (%s, %s, %s)
        """, (part, vector, value))

    def retrieve_request(self):
        # Return the next waiting request (if there is one)
        self.rddb.execute("""
            SELECT * FROM requests ORDER BY id DESC LIMIT 1
        """)
        if self.rddb.rowcount > 0:
            return self.rddb.fetchall()[0]
        else:
            return False

    def delete_request(self, request_id):
        # Triggered once the current message has done its job to move to the next request
        self.stdb.execute("DELETE FROM requests WHERE id = %s", (request_id))

    def update_config(self, key, value):
        self.states[key] = value

    def read_config(self):
        # Send back the current state on demand
        return self.states
