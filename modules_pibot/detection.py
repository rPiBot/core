from picamera.array import PiRGBArray
from picamera import PiCamera
import time, cv2, sys, threading, os
import RPi.GPIO as GPIO

import numpy as np
from modules_pibot.config import Config

class Detection(threading.Thread):
    running = False

    request = { 'body': 'stopped' }
    directions = { 'green': 'forwards', 'red': 'backwards' }

    LED = { 'r': 13, 'g': 15, 'b': 16 }
    image_count = 0

    colours = {     # Based on an RGB array - not BGR!
        'red': [GPIO.HIGH, GPIO.LOW, GPIO.LOW],
        'green': [GPIO.LOW, GPIO.HIGH, GPIO.LOW],
        'blue': [GPIO.LOW, GPIO.LOW, GPIO.HIGH],
        'white': [GPIO.HIGH, GPIO.HIGH, GPIO.HIGH],
        'yellow': [GPIO.HIGH, GPIO.HIGH, GPIO.LOW],
        'magenta': [GPIO.HIGH, GPIO.LOW, GPIO.HIGH],
        'cyan': [GPIO.LOW, GPIO.HIGH, GPIO.HIGH],
        'off': [GPIO.LOW, GPIO.LOW, GPIO.LOW]
    }

    def __init__(self, config):
        threading.Thread.__init__(self)
        self.running = True
        self.wx, self.wy = 480, 368
        self.deadzone = 0.25  # Amount of screen in center for no movement to occur

        # Clear directory of images from previous run
        folder = '/var/www/ui/images'
        for image in os.listdir(folder):
            image_path = os.path.join(folder, image)
            try:
                if os.path.isfile(image_path):
                    os.unlink(image_path)
            except Exception as e:
                print(e)

        # Set the x-axis points at which movement left/right should begin, rather than just forwards/backwards
        self.movement_deadzone_max = (self.wx/2) - (self.wx*self.deadzone)
        self.movement_deadzone_min = (self.wx/2) + (self.wx*self.deadzone)

        # Prepare the camera
        self.camera = PiCamera()
        self.camera.resolution = (self.wx,self.wy)
        self.camera.rotation = 180         # Rotate the image as the camera is mounted upside down
        self.camera.framerate = 10
        self.raw = PiRGBArray(self.camera, size=(self.wx,self.wy))

        GPIO.setmode(GPIO.BOARD)
        for pin in self.LED:
            GPIO.setup(self.LED[pin], GPIO.OUT)
            GPIO.output(self.LED[pin], GPIO.LOW)

        # Flash the front LED to indicate we're using detection mode
        for x in range(0, 10):
            self.led_change('off')
            time.sleep(0.1)
            self.led_change('white')
            time.sleep(0.05)

    def __exit__(self):
        self.running = False
        for pin in self.LED:
            GPIO.output(self.LED[pin], GPIO.LOW)

    def run(self):
        config = Config()
        self.detection(config)

    def led_change(self, colour):
        values = {'r': self.colours[colour][0], 'g': self.colours[colour][1], 'b': self.colours[colour][2]}
        for c in values:
            GPIO.output(self.LED[c], values[c])

    def colour_detection(self, hsv):
        green = cv2.inRange(hsv,np.array((27,82,40)),np.array((45,255,255)))
        red = cv2.inRange(hsv,np.array((165,155,30)),np.array((190,255,255)))
        return cv2.add(green, red)

    # Decide what to do after having found a potential target
    def contour_decision(self, image, x, y, r, colour):
        self.led_change(colour)
        direction = 'stopped'                # Safe default

        if x and y and r:

            direction = self.directions[colour]

            # Clear the direction prefix if we're only going left or right, not forwards/backwards as well
            if (x < self.wx / 8) or (x > self.wx / 8 * 7):
                direction = ''
            elif x <= self.movement_deadzone_max or x >= self.movement_deadzone_min:
                direction += '_'

            if x <= self.movement_deadzone_max:
                direction += 'left'
            elif x >= self.movement_deadzone_min:
                direction += 'right'
            else:
                if (r > 80 and colour == 'green') or (r < 25 and colour == 'red'):
                    # Green is close or red far enough away to claim victory
                    direction = 'stopped'

            self.request['message'] = str.upper(colour) + ' ball detected of size ' + str(r) + ' at point ' + str(x) + ' - moving ' + str.upper(direction)

        self.request['body'] = direction    # Send the request over to the parent thread to action
        cv2.putText(image, str.upper(direction), (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))


    def write_image(self, image):
        # Output a file for a browser to view in test mode
        cv2.imwrite("/var/www/ui/images/detected_output"+('%06d' % self.image_count)+".jpg", image)
        self.image_count += 1

    def detection(self, config):
        for frame in self.camera.capture_continuous(self.raw, format="bgr", use_video_port=True):
            # Grab the raw NumPy array representing the image for processing
            image = frame.array

            # Blur slightly to reduce noise
            # image = cv2.medianBlur(image,5)

            # Convert BGR to HSV
            hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

            # Do the colour detection to find the objects we're looking for, then apply filters to help find contours
            screened = self.colour_detection(hsv)

            erode = cv2.erode(screened, None, iterations = 3)
            dilate = cv2.dilate(erode, None, iterations = 10)

            # composite = cv2.bitwise_and(image, image, mask=dilate)

            _, contours, _= cv2.findContours(erode, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

            areas = []
            for i, contour in enumerate(contours):
                (x,y),r = cv2.minEnclosingCircle(contour)
                areas.append({'x':int(x), 'y':int(y), 'r':int(r), 'contour': contour})

            if areas:
                largest = max(areas, key=lambda x:x['r'])	# Find the object found with the largest radius
                x,y,r = largest['x'], largest['y'], largest['r']

                print len(cv2.approxPolyDP(largest['contour'],0.01*cv2.arcLength(largest['contour'],True),True))

                hue = hsv.item(y,x,0)

                colour = False
                line = (255, 255, 255)

                if r > 20:
                    if 140 < hue < 180:                    # RED
                        colour = 'red'
                        line = (0, 0, 255)
                    elif 27 < hue < 45:                    # GREEN
                        colour = 'green'
                        line = (0, 255, 0)

                    if colour:
                        self.contour_decision(image, x, y, r, colour)

                    cv2.circle(image, (x, y), r, line, 3)
                    text = '(' + str(x) + ', ' + str(y) + '), ' + str(r)
                    cv2.putText(image, text, (x-50, y-70), cv2.FONT_HERSHEY_SIMPLEX, 0.5, line)

                else:
                    cv2.putText(image, 'STOPPED - r < 20', (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
            else:
                # Default state - can't see a ball
                if 'message' in self.request:
                    del self.request['message']

                cv2.putText(image, 'STOPPED', (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
                self.request['body'] = 'stopped'
                self.led_change('off')

            self.write_image(image)

            # Clear the raw stream for the next frame
            self.raw.truncate(0)
