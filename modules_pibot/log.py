import logging, os, time
import logging.config

class Log():
    def __init__(self):
        # Clear old log file if it exists
        try:
            os.remove("/home/pi/core/rpibot.log")
        except OSError:
            pass

        logging.config.fileConfig('/home/pi/core/log.conf')

        self.logger = logging.getLogger('rpibotlog')

        self.logger.info("Initialising")
        
    def __exit__(self):
        False

    def insert(self, logging_type, entry):
        logging_types = {
            'debug': self.logger.debug,
            'info': self.logger.info,
            'warning': self.logger.warning
        }
        logging_types[logging_type](entry)
