# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
from modules_pibot.config import Config
import time, random

class Body:
    state = ''
    LEDs = { 'FL': 11, 'FR': 12, 'RL': 8, 'RR': 7 }
    motors = { 'FL': 37, 'FR': 38, 'RL': 36, 'RR': 35 }  # Direction, Position - FL = Forwards Left

    def __exit__(self):
        GPIO.cleanup()

    def __init__(self, config):
        # Prepare the GPIO pins
        GPIO.setmode(GPIO.BOARD)

        for motor in self.motors:
            GPIO.setup(self.motors[motor], GPIO.OUT)

        for LED in self.LEDs:
            GPIO.setup(self.LEDs[LED], GPIO.OUT)

        # Test the LEDs
        self.wiggle_lights(10)
        self.stop(config)

    def __exit__(self):
      self.reset()

      GPIO.cleanup()

    def wiggle_lights(self, count):
        if count > 1:
            GPIO.output(self.LEDs['FL'], GPIO.HIGH)
            GPIO.output(self.LEDs['FR'], GPIO.LOW)
            GPIO.output(self.LEDs['RL'], GPIO.HIGH)
            GPIO.output(self.LEDs['RR'], GPIO.LOW)

            time.sleep(0.1)

            GPIO.output(self.LEDs['FL'], GPIO.LOW)
            GPIO.output(self.LEDs['FR'], GPIO.HIGH)
            GPIO.output(self.LEDs['RL'], GPIO.LOW)
            GPIO.output(self.LEDs['RR'], GPIO.HIGH)

            time.sleep(0.1)

            self.wiggle_lights((count - 1))

    def reset(self):
        # Stop motors and turn off LEDs
        for motor in self.motors:
            GPIO.output(self.motors[motor], False)

        for LED in self.LEDs:
            GPIO.output(self.LEDs[LED], GPIO.LOW)

    def stop(self, config):
        self.reset()
        self.state = 'stopped'
        Config.update_config(config, 'direction', 'stopped')

    def move(self, direction, sensors, config):
        if self.state == 'evading':
            direction = 'stopped'

        allowed = { 'F': True, 'R': True }
        proximity = { 'max': 20, 'min': 10}

        prox = proximity['max'] if self.state != 'stopped' else proximity['min'] # Give time to brake

        for sensor in sensors:
            # Stop if approaching an object
            if sensors[sensor] < prox and sensors[sensor] > proximity['min']:
                allowed[sensor] = False
                if (sensor == 'F' and direction in ['forwards', 'forwards_left', 'forwards_right']) or (sensor == 'R' and direction in ['backwards', 'backwards_left', 'backwards_right']):
                    self.stop(config)

            # Move away from object if too close
            if sensors[sensor] <= proximity['min']:
                allowed[sensor] = False
                self.state = 'evading'
                direction = 'forwards' if sensor == 'R' else 'backwards'

        if allowed['F'] == False and allowed['R'] == False:
            #direction = random.choice(['left', 'right']) #TODO prevent from changing randomised decision for x time so it doesn't 'flip' between avoidance states
            direction = 'left'
            #time.sleep(0.4)

        if direction != self.state: # There's going to be a change from the current direction/state
            if self.state != 'evading':
              self.state = direction

            Config.update_config(config, 'direction', direction)

            self.reset() # Stop motors and LEDs before completing the requested action

            # Now turn on the relevant motors and LEDs based on the direction
            if direction == 'forwards' and allowed['F'] == True:
                GPIO.output(self.motors['FL'], True)
                GPIO.output(self.motors['FR'], True)
                GPIO.output(self.LEDs['FL'], GPIO.HIGH)
                GPIO.output(self.LEDs['FR'], GPIO.HIGH)
            elif direction == 'forwards_left' and allowed['F'] == True:
                GPIO.output(self.motors['FL'], True)
                GPIO.output(self.LEDs['FR'], GPIO.HIGH)
            elif direction == 'forwards_right' and allowed['F'] == True:
                GPIO.output(self.motors['FR'], True)
                GPIO.output(self.LEDs['FL'], GPIO.HIGH)
            elif direction == 'backwards' and allowed['R'] == True:
                GPIO.output(self.motors['RL'], True)
                GPIO.output(self.motors['RR'], True)
                GPIO.output(self.LEDs['RL'], GPIO.HIGH)
                GPIO.output(self.LEDs['RR'], GPIO.HIGH)
            elif direction == 'backwards_left' and allowed['R'] == True:
                GPIO.output(self.motors['RL'], True)
                GPIO.output(self.LEDs['RR'], GPIO.HIGH)
            elif direction == 'backwards_right' and allowed['R'] == True:
                GPIO.output(self.motors['RR'], True)
                GPIO.output(self.LEDs['RL'], GPIO.HIGH)
            elif direction == 'left':
                GPIO.output(self.motors['RL'], True)
                GPIO.output(self.motors['FL'], True)
                GPIO.output(self.LEDs['FR'], GPIO.HIGH)
                GPIO.output(self.LEDs['RL'], GPIO.HIGH)
            elif direction == 'right':
                GPIO.output(self.motors['RR'], True)
                GPIO.output(self.motors['FR'], True)
                GPIO.output(self.LEDs['FL'], GPIO.HIGH)
                GPIO.output(self.LEDs['RR'], GPIO.HIGH)
            else:
                self.stop(config)
