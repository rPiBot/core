from modules_pibot.detection import Detection
from modules_pibot.body import Body
from modules_pibot.config import Config
from modules_pibot.sensors import Sensors
from modules_pibot.log import Log

import time, random

class Actions():
    def __init__(self, config, log):
        self.body = Body(config)

        self.sensors = Sensors()
        self.sensors.daemon = True
        self.sensors.start()

        self.detection_has_started = False

    def __exit__(self, config, log):
        if self.detection_has_started:
          Detection.__exit__(self.detection)
        Body.__exit__(self.body)

    ### x_change methods are called from the main loop if a change to the current behaviour has been requested

    def control_change(self, state, new_request, config, log):
        # Change the control method (e.g. manual control, detection control)
        Config.update_config(config, 'control', new_request['value'])

    def body_change(self, state, new_request, config, log):
        # Send a request to move the main body
        Body.move(self.body, new_request['vector'], Sensors.retrieve(self.sensors), config)

    ### x_action methods are called from the main loop based on which 'control' method is set (in control_change)

    def default_action(self, direction, sensors, config, log):
        # No new request, but the sensor logic needs to be applied for avoidance
        Body.move(self.body, direction, Sensors.retrieve(self.sensors), config)

    def detection_action(self, state, time_in_state, config, log):
        if self.detection_has_started == False:
            self.detection = Detection(config)
            self.detection.daemon = True
            self.detection.start()
            self.detection_has_started = True

        new_request = Detection.request
        direction = state['direction']

        if new_request['body'] != state['direction']:
            direction = new_request['body']

        # Stop movement if no change after 1 second
        if time_in_state >= 2 and state['direction'] != 'stopped':
          Log.insert(log, 'info', 'emergency stop')
          direction = 'stopped'
          Config.update_config(config, 'direction', 'stopped')
          time_in_state = 0

        #TODO pass the log object through to detection.py to log directly from there
        if 'message' in new_request:
            Log.insert(log, 'info', str(new_request['message']))

        self.default_action(direction, self.sensors, config, log)

        return time_in_state

    def random_action(self, state, time_in_state, config, log):
      if state['direction'] == 'stopped' or time_in_state > random.randint(1, 4):
        direction = random.choice(['left', 'right', 'forwards', 'left', 'forwards_left', 'forwards_right', 'backwards_left', 'backwards_right'])
        Config.insert_request(config, 'body', direction, '')

      self.default_action(state['direction'], self.sensors, config, log)

      return time_in_state

    def manual_action(self, state, time_in_state, config, log):
      # Stop movement if there has been no communication for 10 seconds
      if time_in_state > 5 and state['direction'] != 'stopped':
        Config.update_config(config, 'direction', 'stopped')
        time_in_state = 0

      self.default_action(state['direction'], self.sensors, config, log)

      return time_in_state
