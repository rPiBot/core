#!/usr/bin/env python

import os, sys, time, threading, random
from datetime import datetime

from modules_pibot.actions import Actions
from modules_pibot.config import Config
from modules_pibot.log import Log

config = Config()
log = Log()
actions = Actions(config, log)

# Clear the database, but don't include this in _init as can be initialised later by external sources
Config.prepare_database(config)

state = Config.state(config) # Initial state - will be repetitively overridden during the main loop

# The main loop
try:
    Log.insert(log, 'info', 'Ready')
    
    while True:
        try:
            # Keep reading config for changes
            state = Config.state(config)
            new_request = Config.retrieve_request(config)
            time_in_state = state['time_in_state'] + state['delay']

        except Exception as e:
            Log.insert(log, 'warning', e)
        else:               # Successful

            if new_request:
              Config.update_config(config, 'time_in_state', 0)                                                 # Reset the clock
              getattr(Actions, new_request['component']+'_change')(actions, state, new_request, config, log)   # Run the associated action
              Config.delete_request(config, new_request['id'])                                                 # Clear the request from the queue
              if new_request['vector'] and state['control'] != 'detection':
                  Log.insert(log, 'info', str.upper(new_request['vector']))                                    # Log the change in direction ('vector')
              elif new_request['value']:
                  Log.insert(log, 'info', ' NEW CONTROL TYPE: ' + new_request['value'])                        # Log the change in control type ('value')
            else:
              time_in_state = getattr(Actions, state['control']+'_action')(actions, state, time_in_state, config, log)
              Log.insert(log, 'debug', str.upper(state['direction']) + ' for ' + format(time_in_state, '.2f') + ' seconds')
              Config.update_config(config, 'time_in_state', round(time_in_state, 2))                           # Update the time in state if no change

        # Pause for the delay amount (default 0.05, but can be overridden by individual modules if needed)
        time.sleep(state['delay'])

except KeyboardInterrupt:
    # Call the exit methods if ctrl-c is triggered
    Actions.__exit__(actions, config, log)
    exit()
