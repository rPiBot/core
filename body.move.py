# This file is exclusively triggered by the UI, and handles inserting a request into the queue

from modules_pibot.config import Config
import sys

config = Config()

if len(sys.argv) > 1:
    if sys.argv[1] in ('forwards', 'forwards_left', 'forwards_right', 'backwards', 'backwards_left', 'backwards_right', 'left', 'right', 'w', 'a', 's', 'd'):
        if sys.argv[1] in ('w', 'a', 's', 'd'):
            directions = { 'w': 'forwards', 'a': 'left', 's': 'backwards', 'd':'right'}
            direction = directions[sys.argv[1]]
        else:
            direction = sys.argv[1]

        print 'Moving', direction
    else:
        #print 'No valid direction - stopping'
        direction = 'stopped'
else:
    print "No direction given - stopping"
    direction = 'stopped'

Config.insert_request(config, 'body', direction, '')
