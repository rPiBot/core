# rPiBot/core

## Contents

* [Introduction](#markdown-header-introduction)
* [Hardware](#markdown-header-hardware)
    * [Shell](#markdown-header-shell)
    * [Raspberry Pi 3](#markdown-header-raspberry-pi-3)
    * [Motors](#markdown-header-motors)
    * [LEDs](#markdown-header-leds)
    * [HC-SR04 Ultrasonic Range Sensors](#markdown-header-hc-sr04-ultrasonic-range-sensors)
    * [Camera](#markdown-header-camera)
    * [RGB LED](#markdown-header-rgb-led)
* [Software](#markdown-header-software)
* [Code](#markdown-header-code)
    * [Overview](#markdown-header-overview)
    * [Config – Messages in MySQL Database](#markdown-header-config-messages-in-mysql-database)
    * [Sensors](#markdown-header-sensors)
    * [Detection](#markdown-header-detection)
    * [Log](#markdown-header-log)
    * [PHP UI](#markdown-header-php-ui)
* [Future](#markdown-header-future)
    * [Code Improvements](#markdown-header-code-improvements)
    * [General Goals](#markdown-header-general-goals)


## Introduction

This is a project to have a Raspberry Pi based robot control itself based on external elements.  In this case, it moves around based on the visibility of a green ball and a red ball – being attracted to green and repelled by red.  This page consists partly of an area to document the history of the project, and also to give help for anyone that wants to build something similar.

[Watch a demo video here.](https://www.youtube.com/watch?v=ec6LR3AOZ0c)

The intention is to continue to update this now there is a solid foundation, with the view of it eventually becoming a kind of personal assistant.  Ultimately it could become a low-cost robotic 'slave' - finding and retrieving objects and doing general tasks, though this is obviously a very long way away!

[![Watch a demo video](images/red-green.gif)](https://www.youtube.com/watch?v=ec6LR3AOZ0c)

## Hardware
The shopping list:

* £30 	Raspberry Pi 3
* £20 	Raspberry Pi Camera
* £50 	Lego RC Tracked Racer 42065
* £5 	Portable phone charger
* £5 	L298N motor controller module
* £5 	HC-SR04 ultrasonic sensors
* £2 	LEDs – 2 red, 2 white, 1 RGB
* £8 	Kit of various cables, resistors and breadboard

* £125 	Total

![rPiBot](images/side.jpg)


### Shell
A tank-track style vehicle seemed likely to give the most versatility and manoeuvrability.  
The first design for the shell used plasticard to house the electrical components and four motors.  It was a simple box layout, with the sides holding the top and bottom together with screws and angular brackets.  There was space for pan/tilt servos for the camera on the front, which was dropped in the second iteration as it had little use.

![v2](images/v2.png)

This design was useful as a container to house all the components, but it quickly became clear that the approach had limited potential for improvement without significant rework, so switching to Lego made sense.  Lego gave a much more modular approach as it’s obviously easier to prototype and redevelop in an iterative cycle.  The [RC Tracked Racer 42065](https://shop.lego.com/en-US/RC-Tracked-Racer-42065) set became the base for the motors and tank tracks, and the bricks for the top section repurposed to contain the rest of the hardware.

Using only the bottom section of the Lego set to produce the tracks and motors left plenty of remaining bricks to construct a chassis that could contain the hardware.  It does make replacing the AA batteries very fiddly, though – a future update would be to make use of a second portable phone charger style battery that can output 9v and be charged in place.



### Raspberry Pi 3
The intention of the project was to have a relatively inexpensive device that does all of its calculations natively, without needing to transfer to a server for more processing power.  The reason for this is so that it continues to work without a network, and doesn’t just lose all of its abilities if there’s a power or network failure.  

![pi](images/pi.jpg)

A Raspberry Pi is the logical choice for this – they’re small, light, cheap and power efficient.  It does call for compromises in the code given the hardware limitations, however – particularly the memory.  But these limitations are known and it gives a good end-point as to what can be achieved so keeps the scope tight.  It being UNIX based helps open up a large amount of libraries, too.

### Motors
The Lego motors can be controlled by an L298N controller either by cutting off the brick at the end of the cable and branching in to two of the cables, or (as in this project) by carefully inserting cables into the respective connectors on the brick – pins 2 and 3 work well.  To send power to the L298N from the Lego battery pack, the ends of the cable normally going to the IR receiver can be cut off and then insert the required cables to the controller.  This project is using GPIO pins 35 (RR), 36 (RL), 37 (FL) and 38 (FR) to trigger the motors, with a final connection to a ground pin on the Pi from the L298N controller.

![Motors](images/front_front.jpg)

### LEDs
To help give some feedback when developing the software, there are some LEDs that are triggered at the same time as the motors – two white LEDs when going forwards, two red LEDs when backwards actions are triggered.  These are on GPIO pins 7 (RR), 8 (RL), 11 (FL) and 12 (FR).  These could potentially have used the same circuit as the motors, but keeping them independent offers the opportunity for the LEDs to be can be triggered in other circumstances – in this project they’re being used as a BIOS POST style arrangement to show the device is ready once started.  

The main benefit to the LEDs is to be able turn off the motors while testing new code, and still have some visual feedback on when the motors would have triggered.

### HC-SR04 Ultrasonic Range Sensors
To protect itself from running into objects, two HC-SR04 ultrasonic range sensors are used front and rear.  They have two main functions – one to emit an ultrasound sound and the other to retrieve that same wave.  Since we know how fast sound travels and have control over when the sound started, all we need to know is how long it took to bounce off whatever object is in front of it to be able to calculate the distance.

![Sensors](images/hc-sr04.jpg)

The sensors are put together into a circuits using trigger and echo GPIO pins (33 and 40 for the front; 31 and 32 for the rear), together with shared power and ground pins in a modified arrangement from a [ModMyPi](https://www.modmypi.com/blog/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi) tutorial.  

These sensors then take priority over any other actions the robot moves as a safety feature – they will stop forwards motion if an object is within a certain range, and then will move in the opposite direction if that object continues to get closer.


### Camera
The standard Raspberry Pi camera is attached to perform the OpenCV image detection features.  A USB camera could work as well, but the Raspberry Pi camera is small, of a good quality and integrates well.

In an earlier setup, the camera was placed onto pan and tilt servos and utilised the ServoSix board, but in reality the whole robot can spin on the spot with tank tracks, making the x-axis redundant.  Perhaps a servo will return at some point to look up and down, though.


### RGB LED
The front of the robot houses an RGB LED for feedback.  There was a speech module at one point, but the ServoSix module that was moving the camera interfered with the 3.5mm jack for outputting audio.  The latest robot now has neither a moving camera nor speech, so either of these may return at some point.
The RGB LED gives a more visual cue as to what is happening.  It’s set up to turn green or red when it sees the relevant ball, and to give feedback when initialising, but could easily be used for many other features.


## Software
The software was in part dictated by the hardware, in that there are a lot of Python libraries available to make use of the GPIO pins and the Raspberry Pi is relatively underpowered by design  as it’s so small and energy efficient.  Python is also a relatively resource-friendly platform in comparison to some other languages such as Ruby, which by default can be thirsty on memory.   It made sense to stay open-source.  This meant learning Python, so the code in this project is by no means flawless – although realistically very little human code ever can be.

The main setup requires OpenCV to be installed and importable into Python, as well as the Raspberry Pi GPIO module, which should be available in the default Python on the Pi.
Other useful places for inspiration, plus now-unused modules:

* The [OpenCV image manipulation documentation](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html#converting-colorspaces)
* [PyImageSearch](https://www.pyimagesearch.com/2017/10/09/optimizing-opencv-on-the-raspberry-pi/) for getting OpenCV going on the Pi and lots of other hints
* The imutils [range-detector](https://github.com/jrosebr1/imutils/blob/master/bin/range-detector) script for determining HSV colours
* Claude Pageau’s [motion-track](https://github.com/pageauc/motion-track/blob/master/motion-track.py) to output coordinates of the object moving the most – this was great in this project while the robot is static, but obviously becomes less useful when the whole world is moving
* Steven Jacob’s [Xbox 360 controller](https://github.com/FRC4564/Xbox) was put to good use while testing the hardware before other control methods were written

## Code

### Overview
The code is structured largely through experience in other platforms, particularly MVC frameworks like Ruby on Rails.  This approach seems to work well in Python too, especially from a code arrangement point of view, although back-to-back comparisons need to be made to determine performance benefits/losses.  

The _Actions_ class acts as a controller for the “models” of _Body_, _Config_, _Detection_, _Log_ and _Sensors_.  It is all triggered through _start.py_ which houses the main loop.

There’s also a _pibot.sh_ file, which is to set up the main loop as a service when the device is powered on, removing the need to SSH in to start the application.
What follows is an explanation of the decisions made for each main area; it isn’t intended to be a line-by-line deconstruction, although there are plenty of comments within the code that may help with this.

### Config – Messages in MySQL Database
A message-based system queues up requests to perform actions, which are then completed in order.  This removed thread-lock problems or issues where requests were being sent out of order – these are time stamped, and it makes it easier to priorities if multiple messages arrive almost simultaneously.  

MySQL was used for this using the InnoDB engine for row-locking rather than table-locking, so multiple entries can be inserted simultaneously and independently.  The _Config_ module is where all of the MySQL transactions take place; every request for a normal change of action will go through here – whether this be a change in control type, or a direction change for the body.  The PHP UI calls Python scripts that insert messages into the database, and these scripts call the _Config_ class directly, so even areas away from the main loop still have to go through the _Config_ area to have their actions performed.

The sensors are the only exception to this approach, which bypass the message queue entirely since the actions on any logic from these are not to be a request; they are mandatory to prevent a crash.

### Sensors
The _Sensors_ class is running in a separate thread to the main loop so they can act independently.  The ultrasonic range sensors take priority over everything else and to act as a safety net to prevent the robot running into a wall when travelling forwards or backwards – so any request to go forwards will be denied by the output from the front sensor if it’s unsafe because there’s an object in close proximity.  Similarly, if that object continues getting closer, it actively sends a message to move away from it (as opposed to passively instructing the robot to stop).

![Breadboard](images/breadboard.jpg)

It’s a relatively small class that is largely self-explanatory from the comments in the code.  The main thing is that it returns values for the _Body_ class to make use of if the distance figure returned is suitably small to take any action on.

### Detection
_Detection_ is also running in a separate thread, but does not pass any active messages back to the main loop – it merely passes requests to move based on what it found via the camera and OpenCV image detection.
It sets the definitions at the start – setting up the camera, and what size of image and deadzone to set, then goes about looking for the specific green and red colours.  The imtulis range-detector script proved invaluable for finding those HSV values based on an image.  Taking several images in slightly different lighting allowed, then determining those HSV values allowed an average to be calculated that works for most lighting conditions.  It’s obviously going to fall down if attempted in an old fashioned photo printing studio, or a dirty glow-in-the-dark 1990s rave, but the approach works in expected conditions and the red and green balls were detected reliably.  With more processing performance available, perhaps these kinds of environments could be compensated for - but this likely be beyond the reach of the Raspberry Pi at this stage in addition to everything else being asked of it and keep the expectation of close to real-time performance.

The image is first screened for these HSV colours, then 'erode' and 'dilate' methods used to smooth and enhance the masks of the area(s) found.  See the [OpenCV documentation](https://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html) for a detailed explanation on these methods.  Hopefully by this point there are some nice curves for the [findContours](https://docs.opencv.org/2.4/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html?highlight=findcontours#findcontours) method to discover.

![rPiBot](images/rPiBot.gif)

These contours are then placed into an array of areas so we can detail their coordinates and radius, and only the area with the largest radius is taken into account.  This means if both red and green balls are visible, only the closest (largest radius) will be considered.  This is to cut down on the amount of processing done each frame, but with more powerful hardware or a different approach, perhaps this could be improved – maybe to drive around one object to get to the other.

Once it knows there is some red or green in view, it then determines whether this is circular, and what size.  If the radius of this circle is above the specific limit, it can then tell the robot to move.  If it’s the green ball and it’s far away, move forwards.  If on the left part of the screen, rotate the robot to the left.  The next request will come along in 1/10th of a second (as this is the specified frame rate), so it will keep repeating, adjusting the motors, and eventually know to stop the motors entirely – once the green ball is over a certain radius, it will stop.  This will happen just before the point at which the ultrasonic sensors kick in and take evasive action, so it gets as close as it possibly can.

On each pass it sends back the request to the _Body_ to move in whichever direction it determined is the way to achieve the goal of finding green and/or avoiding red.  For feedback, the RGB LED lights red or green, and it will also output the camera image with an inserted circle shape and text to show what was found.  This is displayed in the PHP UI.

### Log
The _Log_ class serves two purposes – to appropriately prioritise outputs from the application and deliver to the supplementary UI.  Initially it is quick and easy to ‘print’ debugging outputs to the screen, but this becomes tedious when you want to temporarily suppress certain messages from the application.  

This is where it becomes far more useful to categorise log messages (DEBUG, INFO, WARNING, ERROR, etc) so you can choose what level you’re interested in viewing at that moment.  It also allows the log messages to be written to a file as well (or instead of) outputting to the console, which means the basic PHP UI can present this to allow users to view messages without SSHing in to the robot.

### PHP UI

![ui](images/ui2.png)

Given that Python is used for the main application, perhaps Django would have been a suitable framework to choose for a UI.  However, PHP was readily available and this UI is a very simple single page affair, so an entire framework would likely have been overkill for the task.

This is a basic page with Bootstrap-based CSS that calls PHP scripts via AJAX, which finally call the _body.move.py_ and _control.change.py_ scripts.  At one point it also sent messages to move the pan/tilt servos before the concept was dropped.  It displays the output from the _Detection_ module when in detection mode, refreshing the image every half a second, and updates the output from the log in a similar way.

The code can be accessed [here](https://bitbucket.org/rPiBot/ui).

![ui](images/ui.png)


## Future

As mentioned in the introduction, the idea is for this to ultimately become a real-world assistant of some kind.  This initial phase is largely to have a proof of concept for a few ideas and to act as a solid base going forwards.  There's a long road before this can happen, including advances in affordable technology, but shorter-term plans can be separated into code improvements and general goals:

### Code Improvements
* Move reliance from the message queue for every request - at least to remove the database element of it.  Only external requests (i.e. from the UI) _must_ go to the queue, but local requests (such as from OpenCV) can skip this.  This is, of course, with the assumption that the processing remains on the unit itself, which is the intention.
* Syntax improvements to tidy up code - some of this has been done as a matter of course, but there's always room for optimisation.  There are a number of methods, particularly in the _Detection_ class that could be improved by breaking them down into smaller methods.
* Set priorities on requests so it can decide how important each action should be.  This would feed into a non-linear approach where everything can be thrown at the central core and it determines which can be safely ignored if there are multiple concurrent requests - a little like how a human would interact with the world.  If occupied reading a book, a person would not wait until finishing the book if it was clear from the other sensors that there was a fire nearby.  Currently this is how the robot behaves, other than the sensors - but it would make sense to have this behaviour for all modules, and have them add a 'weight' to their certainty of the next action from their perspective (with the central core adding a further 'weight' to the module as a whole).
* Non-linear timeline and attitude - currently the main loop is time-based, being called every 0.05 seconds.  It needed to be _something_, but what about actions that take a different amount of time?

### General Goals
* Research into OpenCV - the use at present is barely touching the surface of what it is capable of.
* Learn new shapes and objects - and then use a Google images API lookup to categorise items that are found.
* Speech feedback.
