# This file is exclusively triggered by the UI, and handles inserting a request into the queue

from modules_pibot.config import Config
import sys

config = Config()

if sys.argv[1] == 'manual' or sys.argv[1] == 'random' or sys.argv[1] == 'detection':
  Config.insert_request(config, 'control', '', sys.argv[1])
